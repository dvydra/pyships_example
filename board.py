class Board(object):

    def __init__(self,size):
        """ one the problem shere is that i and j vars are not used. Is there a fix?"""
        self.size = size
        self.rows = []
        for i in range(self.size):
            row = []
            for j in range(self.size):
                row.append(Cell())
            self.rows.append(row)
        self.ships = []


    def tryToFitShipHorizontally(self, ship, x):
        if (x - 1 + ship.size()) > self.size:
            raise Exception("can't fit horizontally")


    def tryToFitShipVertically(self, ship, y):
        if (y - 1 + ship.size()) > self.size:
            raise Exception("can't fit vertiacally")


    def verifyCellIsFree(self, cell):
        if cell.ship != None:
            raise Exception("cell is occupied") #should print out location

    def add_ship(self, ship, x, y):
        if ship.ori() == 'h':
            self.tryToFitShipHorizontally(ship, x)

            for i in range(0, ship.size()):
                cell = self.rows[x+i-1][y-1]
                self.verifyCellIsFree(cell) #TODO probably move this to cell
                cell.ship = ship

        elif ship.ori() == 'v':
            self.tryToFitShipVertically(ship, y)

            for i in range(0, ship.size()):
                cell = self.rows[x-1][y+i-1]
                self.verifyCellIsFree(cell)
                cell.ship = ship

        ship.set_cord(self, (x,y))
        self.ships.append( ship )

    def fire(self,x,y):
        cell = self.cellAt(x,y)
        """if cell.fired:
            raise Exception("can't fire at the same location twice")"""

        cell.fired = True
        if cell.ship == None:
            return 'miss'
        else:
            return 'hit'

    def cellAt(self,x,y):
        return self.rows[x-1][y-1]

    def has_floating_ships(self):
        for ship in self.ships:
            if not ship.is_sunk():
                return True
        return False

class Cell(object):

    def __init__(self):
        self.ship = None
        self.fired = False

    def is_hit(self):
        """Note: no If statement is necessary"""
        return self.ship != None and self.fired

