from board import *
from Renderer import *
from player import *
from random import *

class Game(object):
    def __init__(self,player1,player2):
        self.choose_first_player(player1,player2)

    def choose_first_player(self,player1,player2):
        choice = randint(1,2)
        if choice == 1:
            self.player1 = player1
            self.player2 = player2
        else:
            self.player1 = player2
            self.player2 = player1

        self.player1.opponent = self.player2
        self.player2.opponent = self.player1

    def game_is_over(self):
        return not (self.player1.has_floating_ships() and self.player2.has_floating_ships() )

    def next_player_go(self):
        if self.next_player.go() == 'miss':
            if self.next_player == self.player1:
                self.next_player = self.player2
            else:
                self.next_player = self.player1

    def newGame(self):
        self.player1.set_board(Board(10))
        self.player2.set_board(Board(10))
        self.player1.place_ships()
        self.player2.place_ships()
        self.next_player = self.player1

    def run(self):
        while not self.game_is_over():
            self.next_player_go()

        winner = None
        if self.player1.has_floating_ships():
            winner = self.player1
        else:
            winner = self.player2
        print "\nPlayer " + winner.name + " won!"


    def render(self):
        renderer = Render(self.player1)
        renderer2 = Render(self.player2)
        renderer.print_board()
        renderer2.print_board()


"""if __name__ == '__main__':
    Game(DV1(),DV2()).run()"""