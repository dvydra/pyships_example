from board import *
from ship import *
from player import *

class Render(object):

    def __init__(self, player):
        self.player = player


    def print_board(self):
        print self.player.name
        for y in range(10,0,-1):
            self.print_row(y)
        self.print_x_axis()

    def print_x_axis(self):
        print " ",
        for i in range(1,11):
            print " " + str(i),
        print ' '

    def print_row(self,y):
        print str(y) + " ",
        for x in range(1,11):
            cell = self.player.board.cellAt(x,y)
            if cell.ship != None:
                if cell.is_hit():
                    print "X ",
                else:
                    print cell.ship.char_ind + " ",
            else:
                print (" " * 2),
        print " "

