import unittest
from random import *
from game import *
from player import *
from board import *


class DV1(Player):
    def __init__(self):
        Player.__init__(self,"DV1")
        self.track_board = Board(10)

    def place_ships(self):
        ac = AircraftCarrier().horizontal()
        self.board.add_ship(ac,1,1)

        bs = Battleship().horizontal()
        self.board.add_ship(bs,2,7)

        cr = Cruiser().horizontal()
        self.board.add_ship(cr,1,5)

    def go(self):
        x = randint(1, 10)
        y = randint(1, 10)
        while self.has_been_fired(x,y):
            x = randint(1, 10)
            y = randint(1, 10)

        return self.fire(x,y)

class DV2(Player):
    def __init__(self):
        Player.__init__(self,"DV2")

    def place_ships(self):
        ac = AircraftCarrier().horizontal()
        self.board.add_ship(ac,2,3)

        bs = Battleship().vertical()
        self.board.add_ship(bs,3,4)

        cr = Cruiser().horizontal()
        self.board.add_ship(cr,5,5)

    def go(self):
        x = randint(1, 10)
        y = randint(1, 10)
        while self.has_been_fired(x,y):
            x = randint(1, 10)
            y = randint(1, 10)

        return self.fire(x,y)

class GameTest(unittest.TestCase):
    def setUp(self):
        self.game = Game( DV1(), DV2() )

    def test_run(self):
        self.game.newGame()
        self.game.render()
        self.game.run()
        self.game.render()

if __name__ == '__main__':
    unittest.main()