import unittest
from ship import *
from board import *

class ShipTest(unittest.TestCase):

    def setUp(self):
        self.board = Board(10)
        self.ac = AircraftCarrier().horizontal()
        self.ac_v = AircraftCarrier().vertical()
        self.board.add_ship(self.ac, 6,1)
        self.board.add_ship(self.ac_v, 3,1)

        self.destr = Destroyer().horizontal()
        self.board.add_ship(self.destr, 5,2)

    def test_create_ship(self):
        self.assertEqual(self.ac.size(),5)

    def test_set_orientation(self):
        self.ac.horizontal()
        self.assertEqual(self.ac.ori(),'h')

    def test_is_hit(self):
        self.board.fire(9,1)
        self.assertTrue(self.ac.is_hit())

    def test_is_sunk(self):
        self.board.fire(5,2)
        self.board.fire(6,2)
        self.assertTrue(self.destr.is_sunk())

if __name__ == '__main__':
    unittest.main()