class Ship(object):
    def __init__(self,size,char_ind):
        self._size = size
        self.char_ind = char_ind

    def size(self):
        return self._size

    def horizontal(self):
        self._ori = 'h'
        return self

    def vertical(self):
        self._ori = 'v'
        return self

    def ori(self):
        return self._ori

    def set_cord(self, board , cord):
        self._board = board
        self._cord = cord

    def coordinate(self):
        return self._cord

    def hit_count(self):
        x,y = self._cord[0], self._cord[1]
        hits = 0
        if self._ori == 'h':
            for i in range(self.size()):
                cell = self._board.cellAt(x+i, y)
                if cell.is_hit():
                    hits += 1
        elif self._ori == 'v':
            for i in range(self.size()):
                cell = self._board.cellAt(x, y+i)
                if cell.is_hit():
                    hits += 1
        else:
            raise Exception('impossible case')
        return hits

    def is_hit(self):
        return self.hit_count() > 0

    def is_sunk(self):
        return self.hit_count() == self.size()


class Destroyer(Ship):
    def __init__(self):
        Ship.__init__(self,2,'D')

class Cruiser(Ship):
    def __init__(self):
        Ship.__init__(self,3,'C')

class Submarine(Ship):
    def __init__(self):
        Ship.__init__(self,3,'S')

class Battleship(Ship):
    def __init__(self):
        Ship.__init__(self,4,'B')

class AircraftCarrier(Ship):
    def __init__(self):
        Ship.__init__(self,5,'A')