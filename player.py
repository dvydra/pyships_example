from board import *

class Player(object):
    def __init__(self,name):
        self.name = name

    def set_board(self,board):
        self.board = board

    def has_floating_ships(self):
        return self.board.has_floating_ships()

    def has_been_fired(self, x, y):
        return self.opponent.board.cellAt(x,y).fired

    def fire(self,x,y):
        return self.opponent.board.fire(x,y)

