import unittest
from board import *
from ship import *

class BoardTest(unittest.TestCase):

    def setUp(self):
        self.board = Board(10)
        self.ac = AircraftCarrier().horizontal()
        self.ac_v = AircraftCarrier().vertical()
        self.board.add_ship(self.ac, 6,1)
        self.board.add_ship(self.ac_v, 1,6)

    def test_add_ac_horiz(self):
        self.assertEqual(self.board.cellAt(6,1).ship, self.ac)

    def test_add_ac_vert(self):
        self.assertEqual(self.board.cellAt(1,6).ship, self.ac_v)

    def test_add_ac_horiz_out_of_bounds(self):
        self.assertRaises(Exception, self.board.add_ship, self.ac, 7, 1 )

    def test_add_ac_vert_out_of_bounds(self):
        self.assertRaises(Exception, self.board.add_ship, self.ac_v, 1, 7 )

    def test_add_ac_conflict_horizantal(self):
        ac_err = AircraftCarrier().horizontal()
        self.assertRaises(Exception, self.board.add_ship, ac_err, 2, 1 )

    def test_add_ac_conflict_vertical(self):
        ac_err = AircraftCarrier().vertical()
        self.assertRaises(Exception, self.board.add_ship, ac_err, 6, 1 )

    def test_no_ship(self):
        self.assertEqual(self.board.cellAt(1,1).ship, None)

    def test_ship_horizontal(self):
        self.board.add_ship(self.ac, 1,1)
        self.assertEquals(self.board.cellAt(2,1).ship, self.ac)

    def test_ship_vertical(self):
        self.board.add_ship(self.ac_v, 1,1)
        self.assertEquals(self.board.cellAt(1,1).ship, self.ac_v)
        self.assertEquals(self.board.cellAt(1,2).ship, self.ac_v)

    def test_ship_cord(self):
        self.board.add_ship(self.ac_v, 1,1)
        self.assertEquals(self.ac_v._board, self.board)
        self.assertEquals(self.ac_v.coordinate(),(1,1))

    def test_miss(self):
        self.assertEquals(self.board.fire(1,2),'miss')
        self.assertEquals(self.board.cellAt(1,2).ship, None)
        self.assertEquals(self.board.cellAt(1,2).fired, True)

    def test_hit(self):
        self.board.add_ship(self.ac, 1,1)
        self.assertEquals(self.board.fire(1,1),'hit')
        self.assertEquals(self.board.cellAt(1,1).ship, self.ac)
        self.assertEquals(self.board.cellAt(1,1).fired, True)

    def test_has_floating_ships(self):
        self.assertTrue(self.board.has_floating_ships())


    """def test_double_hit(self):
        self.board.add_ship(self.ac, 1,1)
        self.assertEquals(self.board.fire(1,1),'hit')
        self.assertRaises(Exception, self.board.fire, 1, 1 )"""


if __name__ == '__main__':
    unittest.main()